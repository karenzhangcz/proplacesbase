package edu.uchicago.gerber.proplacesbase.yelpapi2;

import android.util.Log;

import com.google.gson.Gson;

import org.scribe.builder.ServiceBuilder;
import org.scribe.model.OAuthRequest;
import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.model.Verb;
import org.scribe.oauth.OAuthService;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import edu.uchicago.gerber.proplacesbase.model.YelpResultsData;



/**
 * Created by Adam Gerber on 5/20/2014.
 * University of Chicago
 */
//see instructions for the classes you need here: http://stackoverflow.com/questions/6676733/yelp-integration-in-android

/**
 * Example for accessing the Yelp API.
 */
public class Yelp {

    private static final String USER_AGENT = "Mozilla/5.0";
    //http://www.mkyong.com/java/how-to-send-http-request-getpost-in-java/
    public static YelpResultsData searchYelp(String location, String searchterm) throws Exception {

        URL obj = new URL(String.format("https://finalproject.4r36guosh8tla.us-east-1.cs.amazonlightsail.com/%s/%s", location, searchterm));
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        con.setRequestMethod("GET");
        con.setRequestProperty("User-Agent", USER_AGENT);
        int responseCode = con.getResponseCode();
        Log.d("UUTT", String.valueOf(responseCode));
        if (responseCode == 200){
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            String res = response.toString();
            YelpResultsData yelpResultData = new Gson().fromJson(res, YelpResultsData.class);
            return yelpResultData;
        } else {
            return null;
        }

    }


//    //these are my developers keys, please get your own by going to: http://www.yelp.com/developers/getting_started/api_access
//    private static final String CONSUMER_KEY = "dSZgGbpE51gcJ2mPFy8Dag";
//    private static final String CONSUMER_SECRET = "CAe7Yp1NEYVPh2Z2ZpDDetqUpWM";
//    private static final String TOKEN = "ksJ-aFEUA-sO8YKI9TwbTem8DoLOOtH0";
//    private static final String TOKEN_SECRET = "O1oqDGf93zFEz-_ctYgicO1VYQM";
//
//    OAuthService service;
//    Token accessToken;

//        /**
//         * Setup the Yelp API OAuth credentials.
//         *
//         * OAuth credentials are available from the developer site, under Manage API access (version 2 API).
//         *
//         * @param consumerKey Consumer key
//         * @param consumerSecret Consumer secret
//         * @param token Token
//         * @param tokenSecret Token secret
//         */
//        public Yelp(String consumerKey, String consumerSecret, String token, String tokenSecret) {
//            this.service = new ServiceBuilder().provider(YelpApi2.class).apiKey(consumerKey).apiSecret(consumerSecret).build();
//            this.accessToken = new Token(token, tokenSecret);
//        }

//    public Yelp(){
//        this.service = new ServiceBuilder().provider(YelpApi2.class).apiKey(CONSUMER_KEY).apiSecret(CONSUMER_SECRET).build();
//        this.accessToken = new Token(TOKEN, TOKEN_SECRET);
//    }


//    public YelpResultsData searchMultiple(String searchTerm, String city) {
//
//         Execute a signed call to the Yelp service.
//        OAuthService service = new ServiceBuilder().provider(YelpApi2.class).apiKey(CONSUMER_KEY).apiSecret(CONSUMER_SECRET).build();
//        Token accessToken = new Token(TOKEN, TOKEN_SECRET);
//        OAuthRequest request = new OAuthRequest(Verb.GET, "http://api.yelp.com/v2/search");
//
//        request.addQuerystringParameter("location", city);
//        request.addQuerystringParameter("term", searchTerm);
//        request.addQuerystringParameter("limit", "20");
//
//        service.signRequest(accessToken, request);
//        Response response = request.send();
//        String rawData = response.getBody();
//
//        YelpResultsData mYelpSearchResult = null;
//
//        try {
//            mYelpSearchResult = new Gson().fromJson(rawData, YelpResultsData.class);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return mYelpSearchResult;
//    }




}