package edu.uchicago.gerber.proplacesbase.presenter;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.telephony.PhoneNumberUtils;

import java.util.Date;

import edu.uchicago.gerber.proplacesbase.model.Place;
import edu.uchicago.gerber.proplacesbase.view.interfaces.PlaceView;
import io.realm.Realm;
import io.realm.RealmResults;


public class PlacePresenter extends Presenter<PlaceView> {

    RealmResults<Place> places;

    @Override
    public void onStart() {
        view.displayPlaces(places);
    }


    @Override
    public void onResume() {
//        places = Realm.getInstance(getView().provideContext())
//                .where(Place.class)
//                .findAllSorted("timestamp", Sort.DESCENDING);
//
//        view.displayPlaces(places);
    }

    @Override
    public void onStop() {


    }


    public void editPlace(Place place) {
        //implementation here
    }

    public void sharePlace(Place placeClicked) {

        String strSubject = "Check out: " + placeClicked.getName();
        String strMessage = "\n\n"; //give the user some room to type a message
        strMessage += "Restaurant: " + placeClicked.getName();
        strMessage += "\nAddress: " + placeClicked.getAddress() + ", " + placeClicked.getCity();
        strMessage += " \n\nPhone: " + PhoneNumberUtils.formatNumber(placeClicked.getPhone());
        strMessage += " \nYelp page: " + placeClicked.getYelp();
        if (placeClicked.getFavorite() == 1) {
            strMessage += "\n[This is one of my favorite restaurants]";
        }
        strMessage += "\n\nPowered by Favorite Places on Android by Adam Gerber";

        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, strSubject);
        sharingIntent.putExtra(Intent.EXTRA_TEXT, strMessage);
        view.provideContext().startActivity(Intent.createChooser(sharingIntent, "Choose client"));
    }

    public void mapPlace(Place place) {
        Intent intentMapOf = new Intent(Intent.ACTION_VIEW,
                Uri.parse("geo:0,0?q=" + place.getAddress() + " " + place.getCity()));
        view.provideContext().startActivity(intentMapOf);
    }

    public void dialPlace(Place place) {
        Intent intentDial = new Intent(Intent.ACTION_CALL);
        intentDial.setData(Uri.parse("tel:" + place.getPhone()));
       // view.provideContext().startActivity(intentDial);
    }

    public void yelpSite(Place place) {
        Intent intentYelp = new Intent(Intent.ACTION_VIEW);
        intentYelp.setData(Uri.parse(place.getYelp()));
        view.provideContext().startActivity(intentYelp);
    }

    public void navigateTo(Place place) {
        Intent intentNavTo = new Intent(Intent.ACTION_VIEW,
                Uri.parse("google.navigation:q=" + place.getAddress() + " " + place.getCity()));
        view.provideContext().startActivity(intentNavTo);
    }

    public void deletePlace(Place place) {
        Realm realm = Realm.getInstance(view.provideContext());
        realm.beginTransaction();
        place = realm.where(Place.class).equalTo("id", place.getId()).findFirst();
        place.removeFromRealm();
        realm.commitTransaction();
        view.displayPlaces(places);
    }

    public void toggleFavorite(Place place) {

       Realm realm = Realm.getInstance(view.provideContext());
        realm.beginTransaction();
        place = realm.where(Place.class).equalTo("id", place.getId()).findFirst();

        if (place.getFavorite() == 0) {
            place.setTimestamp(new Date().getTime());
        }
        place.setFavorite(place.getFavorite() == 0 ? 1 : 0);

        realm.commitTransaction();
        view.displayPlaces(places);

    }

    public void showError(String strErrorMessage){

        final AlertDialog.Builder builder = new AlertDialog.Builder(view.provideContext());
        builder.setTitle("Error")
                .setMessage(strErrorMessage)
                .setCancelable(false);

        builder.setPositiveButton("Close", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                dialog.cancel();
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }


}
