package edu.uchicago.gerber.proplacesbase.presenter.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;



import com.bumptech.glide.Glide;

import edu.uchicago.gerber.proplacesbase.R;
import edu.uchicago.gerber.proplacesbase.model.Place;

public final class PlaceAdapter extends RealmAdapter<Place, PlaceAdapter.ViewHolder> {

    private Context mContext;

    public static class ViewHolder extends RealmAdapter.ViewHolder {
        public TextView listText;
        public TextView listCity;
        public ImageView listNiv;
        public View listTab;

        public ViewHolder(LinearLayout container) {
            super(container);
            this.listText = (TextView) container.findViewById(R.id.list_text);
            this.listCity = (TextView) container.findViewById(R.id.list_city);
            this.listNiv = (ImageView) container.findViewById(R.id.list_niv);
            this.listTab = container.findViewById(R.id.list_tab);
        }
    }

    public interface OnItemClickListener {
         void onItemClick(Place example);
    }

    private final LayoutInflater inflater;
    private final OnItemClickListener listener;

    public PlaceAdapter(Context context, OnItemClickListener listener) {
        this.inflater = LayoutInflater.from(context);
        this.mContext = context;
        this.listener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.restos_row, parent, false);
        ViewHolder vh = new ViewHolder((LinearLayout) v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {


        final Place item = getItem(position);
        //todo populate the fields here.
        holder.listText.setText(item.getName());
        holder.listCity.setText(item.getCategories() + " : " + item.getAddress() + " : "+ item.getCity() );


        Glide.with(mContext)
                .load(item.getImageUrl())
                .placeholder(R.drawable.gear_dark)
                .into(holder.listNiv);


        holder.listTab.setBackgroundColor(item.getFavorite() == 0 ? Color.CYAN : Color.RED);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(item);
            }
        });
    }

}