package edu.uchicago.gerber.proplacesbase.view.activities;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;


import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.material.navigation.NavigationView;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import edu.uchicago.gerber.proplacesbase.R;
import edu.uchicago.gerber.proplacesbase.model.YelpResultsData;
import edu.uchicago.gerber.proplacesbase.utils.PrefsMgr;
import edu.uchicago.gerber.proplacesbase.view.frags.NewFragment;
import edu.uchicago.gerber.proplacesbase.view.frags.PlaceFragment;
import edu.uchicago.gerber.proplacesbase.view.frags.ResultsFragment;
import io.realm.Realm;
import io.realm.RealmConfiguration;

public class NavActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {







    public static final String VERSION_KEY = "VERSION_KEY";
    //change the version number (to any integer other than the version integer stored in SharedPrefs) to reset the Realm DB
    public static final int VERSION = 2;

    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

     // Intent intent = getIntent();


        int storedVersion = PrefsMgr.getInt(this, VERSION_KEY, 0 );
        if (storedVersion == 0 || storedVersion != VERSION){
            resetRealm();
            PrefsMgr.setInt(this, VERSION_KEY, VERSION);
        }

        setContentView(R.layout.activity_nav);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        if (savedInstanceState == null) {
            // create fragment for collection edit buttons
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragment_container, PlaceFragment.newInstance(), PlaceFragment.class.getName())
                    .commit();

        }
    }

    //only use this if you need to make a serious scheme change
    private void resetRealm() {
        RealmConfiguration realmConfig = new RealmConfiguration
                .Builder(this)
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.deleteRealm(realmConfig);
    }

    public void startProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    public void stopProgress() {
        progressBar.setVisibility(View.GONE);
    }

    public void showToast(String text) {
        Toast.makeText(this, text, Toast.LENGTH_LONG).show();
    }

    public void showEditFragment() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction ft = fragmentManager.beginTransaction();
        ft.replace(R.id.fragment_container, NewFragment.newInstance(null));
        ft.addToBackStack(NewFragment.class.getName());
        ft.commit();
    }

    public void showResultFragment(YelpResultsData resultData) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction ft = fragmentManager.beginTransaction();
        ft.replace(R.id.fragment_container, ResultsFragment.newInstance(resultData));
        ft.addToBackStack(NewFragment.class.getName());
        ft.commit();
    }


    public void showPlacesFragment() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        FragmentTransaction ft = fragmentManager.beginTransaction();
        ft.add(R.id.fragment_container, PlaceFragment.newInstance());
        ft.commit();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.nav, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
            //this is just a test to demonstrate that we can gain access to the user's SMS.
            //If you want to send a text giving a certain number (for example 789) then you

            Uri sentUri = Uri.parse("content://sms/sent");
            String[] reqCols = new String[]{"_id", "address", "body"};
            ContentResolver contentResolver = getContentResolver();
            Cursor cursor = contentResolver.query(sentUri, reqCols, null, null, null);
            cursor.moveToFirst();

            Toast.makeText(this,cursor.getString(1) + " : " + cursor.getString(2),
                    Toast.LENGTH_SHORT ).show();
            Log.d("UUTT", cursor.getString(1) + " : " + cursor.getString(2));




        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
