package edu.uchicago.gerber.proplacesbase.view.interfaces;


public interface ResultView extends BaseView{

    //there are no methods in this interface yet, but it will still inheret the BaseView ones.
    //You can add contract methods if you need them.
}
