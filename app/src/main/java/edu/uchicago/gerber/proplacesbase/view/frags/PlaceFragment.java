package edu.uchicago.gerber.proplacesbase.view.frags;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;

import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;


import com.google.android.material.floatingactionbutton.FloatingActionButton;

import edu.uchicago.gerber.proplacesbase.R;
import edu.uchicago.gerber.proplacesbase.model.Place;
import edu.uchicago.gerber.proplacesbase.model.SoundVibeUtils;
import edu.uchicago.gerber.proplacesbase.presenter.PlacePresenter;
import edu.uchicago.gerber.proplacesbase.presenter.adapters.PlaceAdapter;
import edu.uchicago.gerber.proplacesbase.view.interfaces.PlaceView;
import io.realm.Realm;
import io.realm.RealmResults;


public class PlaceFragment extends BaseFragment<PlacePresenter> implements PlaceView {

    private PlaceAdapter mAdapter;


    public PlaceFragment() {
    }

    public static PlaceFragment newInstance() {
        PlaceFragment fragment = new PlaceFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_place_list, container, false);

        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //call back to the single Activity that hosts all frags
                //when .showEditFragment() is shown, then onStart(), onResume() and eventually onStop() will be called
                getNavActivity().showEditFragment();
            }
        });


        mAdapter = new PlaceAdapter(getActivity(), new PlaceAdapter.OnItemClickListener() {
            public void onItemClick(final Place placeClicked) {


                //this is just an example, I would put this elsewhere
                SoundVibeUtils.playSound(getActivity(), R.raw.power_up);

                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

                ListView modeList = new ListView(getActivity());

                String[] stringArray = new String[]{
                        "Edit ",  //0
                        "Share ", //1
                        "Map of ", //2
                        "Dial ",  //3
                        "Yelp site ",  //4
                        "Navigate to ",  //5
                        "Delete ", //6
                        "Cancel ",//7
                        placeClicked.getFavorite() == 1 ? "Unfavorite this Place" : "Favorite this Place" //8

                };

                ArrayAdapter<String> modeAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, android.R.id.text1, stringArray);
                modeList.setAdapter(modeAdapter);
                builder.setView(modeList);
                final Dialog dialog = builder.create();


                Window window = dialog.getWindow();
                window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                window.setGravity(Gravity.CENTER);
                dialog.setTitle(placeClicked.getName());
                dialog.show();
                modeList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        // FavActionUtility favActionUtility = new FavActionUtility(getActivity());
                        Realm realm;
                        Place place;

                        try {
                            switch (position) {
                                case 0:
                                    //edit
                                    //does nothing yet, will need to be implemented.
                                    presenter.editPlace(placeClicked);

                                    break;
                                case 1:
                                    //share
                                    presenter.sharePlace(placeClicked);
                                    break;

                                case 2:
                                    //map of
                                    presenter.mapPlace(placeClicked);
                                    break;
                                case 3:
                                    //dial
                                    presenter.dialPlace(placeClicked);
                                    break;
                                case 4:
                                    //yelp site
                                    presenter.yelpSite(placeClicked);
                                    break;
                                case 5:
                                    //navigate to
                                    presenter.navigateTo(placeClicked);
                                    break;
                                    //delete
                                case 6:
                                    presenter.deletePlace(placeClicked);
                                    break;
                                case 7:
                                    //cancel
                                    break;

                                //toggle the favorite
                                case 8:
                                   presenter.toggleFavorite(placeClicked);

                                    break;
                            }
                        } catch (Exception e) {
                            //gracefully handle exceptions
                            e.printStackTrace();
                            presenter.showError( e.getMessage());

                        }

                        dialog.dismiss();
                    }
                });

            }
        });

        ListView listView = (ListView) view.findViewById(R.id.reminders_list_view);
        listView.setAdapter(mAdapter);

        return view;
    }

    @Override
    protected PlacePresenter createPresenter() {
        return new PlacePresenter();
    }


    //this method is defined in the PlaceView interface and is called from the presenter (PlacesPresenter)
    @Override
    public void displayPlaces(RealmResults<Place> places) {

        mAdapter.setResults(places);
    }


    @Override
    public void startProgress() {
        //this would be called from the presenter
    }

    @Override
    public void stopProgress() {
        //this would be called from the presenter
    }
}
