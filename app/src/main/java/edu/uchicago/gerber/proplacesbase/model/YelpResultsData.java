package edu.uchicago.gerber.proplacesbase.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


// See an example or raw json results here: http://www.yelp.com/developers/documentation/v2/search_api#rValue
//copy and paste into here: //http://jsonviewer.stack.hu/  to see the structure
public class YelpResultsData implements Serializable {


    public List<com.example.Business> businesses;



    public ArrayList<String> getSimpleValues() {
        ArrayList<String> simpleValues = new ArrayList<String>();

        try {
            for (com.example.Business biz : businesses) {

                simpleValues.add(biz.getName() + " | " + biz.getLocation().getAddress1() + " | " + getStars(biz.getRating().toString()));
            }
        } catch (Exception e) {
            //will continue on its own
            return null;
        }
        return simpleValues;
    }

    private String getStars(String strUrl) {

        int nHalfCode = Integer.parseInt("00BD", 16);
        char[] cChars = Character.toChars(nHalfCode);
        String strHalf = String.valueOf(cChars);


        //try the most specific conditions first
        if (strUrl.contains("stars_4_half")) {
            return "****" + strHalf;
        } else if (strUrl.contains("stars_3_half")) {
            return "***" + strHalf;
        } else if (strUrl.contains("stars_2_half")) {
            return "**" + strHalf;
        } else if (strUrl.contains("stars_1_half")) {
            return "*" + strHalf;
        } else if (strUrl.contains("stars_5")) {
            return "*****";
        } else if (strUrl.contains("stars_4")) {
            return "****";
        } else if (strUrl.contains("stars_3")) {
            return "***";
        } else if (strUrl.contains("stars_2")) {
            return "**";
        } else if (strUrl.contains("stars_1")) {
            return "*";
        } else {
            return "";
        }
    }


}
