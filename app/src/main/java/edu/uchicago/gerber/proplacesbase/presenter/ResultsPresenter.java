package edu.uchicago.gerber.proplacesbase.presenter;

import android.util.Log;

import java.util.Date;

import edu.uchicago.gerber.proplacesbase.model.Place;
import edu.uchicago.gerber.proplacesbase.view.interfaces.ResultView;
import io.realm.Realm;
import io.realm.RealmObject;


public class ResultsPresenter extends Presenter<ResultView> {


    public void saveClickedItem(com.example.Business business) {
        com.example.Location location = new com.example.Location();

        location.setAddress1(business.getLocation().getAddress1());
//        location.setAddress2();
//        location.setAddress3();
//        location.setCity();
//        location.setCountry();
//        location.setDisplayAddress();
//        location.setState();
//        0,
//                business.get(),
//                business.getLocation().getCity(),
//                business.getPhone(),
//                business.getUrl(),
//                business.getImageUrl(),
//                business.getCategories().get(0).toString(),
//                new Date().getTime()
//        );

        Realm.getInstance(getView().provideContext()).beginTransaction();
        Realm.getInstance(getView().provideContext()).copyToRealm(location);
        Realm.getInstance(getView().provideContext()).commitTransaction();

    }

    @Override
    public void onStart() {

        Log.d("UURR", "onStart");
    }

    @Override
    public void onResume() {
        Log.d("UURR", "onResume");
    }

    @Override
    public void onStop() {
        Log.d("UURR", "onStop");
    }


}
