package edu.uchicago.gerber.proplacesbase.view.frags;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognizerIntent;

import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Locale;

import androidx.annotation.Nullable;
import edu.uchicago.gerber.proplacesbase.R;
import edu.uchicago.gerber.proplacesbase.model.Place;
import edu.uchicago.gerber.proplacesbase.model.YelpResultsData;
import edu.uchicago.gerber.proplacesbase.presenter.NewPresenter;
import edu.uchicago.gerber.proplacesbase.view.interfaces.NewView;


public class NewFragment extends BaseFragment<NewPresenter> implements NewView {
    public static final int REQUEST_CODE_TEXT = 1001;
    public static final int REQUEST_CODE_SPEECH = 1002;

    private int mItemid = -1; //-1 denotes no item selected

    private ScrollView mRootViewGroup;
    private EditText mNameField, mCityField, mAddressField, mPhoneField, mYelpField;
    private ImageView mPhotoView;
    private TextView mPhoneText, mAddressText, mYelpText;
    private Button mSpeechButton, mExtractButton;

    //the restaurant passed into this activity during edit operation
    private Place mPlace;

    private String mStrImageUrl = "";

    //this is a proxy to our database
    private InputMethodManager mImm;
    private ProgressDialog progressDialog;

    EditText.OnKeyListener enterListener = new EditText.OnKeyListener() {

        @Override
        public boolean onKey(View v, int keyCode, KeyEvent event) {
            if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
                // Perform action on key press

                mImm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
                return true;
            }
            return false;
        }
    };


    public static NewFragment newInstance(Place restaurant) {
        NewFragment fragment = new NewFragment();
        Bundle args = new Bundle();
        args.putSerializable("RESTO", restaurant);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.frag_scroll_new, container, false);

        mPlace = (Place) getArguments().getSerializable("RESTO");

        mRootViewGroup = (ScrollView) v.findViewById(R.id.data_root_view_group_edit);
        mNameField = (EditText) v.findViewById(R.id.restaurant_name);
        mCityField = (EditText) v.findViewById(R.id.restaurant_city);

        //each required field must monitor itself and other text field
        mExtractButton = (Button) v.findViewById(R.id.extract_yelp_button);
        mSpeechButton = (Button) v.findViewById(R.id.speech_search);


        mImm = (InputMethodManager) getActivity().getSystemService(
                Context.INPUT_METHOD_SERVICE);
        //listen for enter and save the record
        mNameField.setOnKeyListener(enterListener);
        mCityField.setOnKeyListener(enterListener);

        mSpeechButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //you will need to implement this
                promptSpeechInput();
            }
        });

//        //button behaviors
        mExtractButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //hide soft keyboard
                presenter.performSearch(mNameField.getText().toString(), mCityField.getText().toString());
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(mCityField.getWindowToken(), 0);
                imm.hideSoftInputFromWindow(mNameField.getWindowToken(), 0);
            }
        });

        return v;
    }




    @Override
    protected NewPresenter createPresenter() {
        return new NewPresenter();
    }

    @Override
    public void startProgress() {
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setTitle("Fetching data");
        progressDialog.setMessage("One moment please...");
        progressDialog.setCancelable(true);

        progressDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                presenter.cancelSearch();
                progressDialog.dismiss();
            }
        });
        progressDialog.show();
    }

    @Override
    public void stopProgress() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void showSearchResult(YelpResultsData results) {
        //call back to the single Activity that hosts all frags
        //when .showResultFragment() is shown, then onStart(), onResume() and eventually onStop() will be called
        getNavActivity().showResultFragment(results);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            if (resultCode == Activity.RESULT_OK) {
                switch (requestCode) {
                    case REQUEST_CODE_SPEECH:
                        if (resultCode == Activity.RESULT_OK && null != data) {
                            ArrayList<String> result = data
                                    .getStringArrayListExtra
                                            (RecognizerIntent.EXTRA_RESULTS);
                            presenter.performSearch(result.get(0));
                        }
                        break;

                }


            } else if (resultCode == Activity.RESULT_CANCELED) {
                //do nothing
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void promptSpeechInput() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT,
                "Say Place Name and Location");
        try {
            startActivityForResult(intent, REQUEST_CODE_SPEECH);
        } catch (ActivityNotFoundException a) {

        }
    }


}

